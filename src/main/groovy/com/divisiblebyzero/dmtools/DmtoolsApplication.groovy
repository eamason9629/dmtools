package com.divisiblebyzero.dmtools

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DmtoolsApplication {

	static void main(String[] args) {
		SpringApplication.run(DmtoolsApplication, args)
	}

}
